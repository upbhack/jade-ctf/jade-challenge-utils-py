import threading
from time import time
from typing import Any, Callable, Dict, Optional
import logging

logger = logging.getLogger(__name__)


class FuncProxy:
    def __init__(
        self,
        orig_fun: Callable[..., Any],
        timeout_call: float,
        timeout_ret: float,
    ):
        self.last_called = time()
        self.last_returned = self.last_called
        self.timeout_call = timeout_call
        self.timeout_ret = timeout_ret
        self.orig_fun = orig_fun
        self._is_timeout_handled = False
        self._lock = threading.Lock()

    def __call__(self, *args, **kwargs):
        self.last_called = time()
        with self._lock:
            self._is_timeout_handled = False

        ret = self.orig_fun(*args, **kwargs)

        self.last_returned = time()
        with self._lock:
            self._is_timeout_handled = False

        return ret

    def is_timed_out(self):
        next_to = self.next_timeout()
        return next_to is not None and next_to <= 0

    def timeout_handled(self):
        with self._lock:
            self._is_timeout_handled = True

    def next_timeout(self):
        with self._lock:
            if self._is_timeout_handled:
                return None
        timeouts = []
        if self.timeout_call > 0:
            timeouts.append(self.timeout_call-(time()-self.last_called))
        if self.timeout_ret > 0:
            timeouts.append(self.timeout_ret-(time()-self.last_returned))
        return max(timeouts)


class TimeoutDetector(threading.Thread):
    """Main class to automate timeout detection
    """

    def __init__(self, callback: Callable[..., bool], *args, **kwargs):
        super().__init__(daemon=True)
        self.should_stop = False
        self._cond = threading.Condition()
        self._callback = callback
        self._args = args
        self._kwargs = kwargs
        self._watched: Dict[Callable[..., Any], FuncProxy] = {}

    def wrap(
        self,
        function: Callable[..., Any],
        timeout_call_seconds: float,
        timeout_ret_seconds: Optional[float] = None,
    ) -> FuncProxy:
        if timeout_call_seconds is None:
            raise ValueError('timeout_call_seconds cannot be None')

        if timeout_ret_seconds is None:
            timeout_ret_seconds = timeout_call_seconds

        if timeout_call_seconds <= 0 and timeout_ret_seconds <= 0:
            raise ValueError('At least one timeout (in seconds) has to be > 0')

        with self._cond:
            if function in self._watched:
                raise ValueError('Function {!r} is already wrapped'.format(function))
            ret = FuncProxy(function, timeout_call_seconds, timeout_ret_seconds)
            self._watched[function] = ret
            self._cond.notify_all()
        return ret

    def unwrap(self, proxied: FuncProxy) -> Callable[..., Any]:
        with self._cond:
            if not isinstance(proxied, FuncProxy):
                raise TypeError('Given function is no FuncProxy')
            if proxied.orig_fun not in self._watched:
                raise ValueError('Function {!r} is not wrapped (by us)'.format(proxied.orig_fun))
            del self._watched[proxied.orig_fun]
            # it is not needed to notify cond here
            # it might timeout too early and notice it has nothing to do
            # so why wake it up and force it to notice that it has nothing to do
            return proxied.orig_fun

    def is_timed_out(self):
        next_to = self.next_timeout()
        return next_to is not None and next_to <= 0

    def next_timeout(self):
        if not self._watched:
            # we watch nothing...
            return None
        return max(
            filter(
                lambda x: x is not None,
                map(FuncProxy.next_timeout, self._watched.values())
            ))

    def run(self):
        logger.debug('Started')
        while not self.should_stop:
            with self._cond:
                # get time till next timeout (to), bounded by 0<= to <= 3600 (1h)
                next_to = self.next_timeout()
                logger.debug('Next timeout in %r', next_to)
                if next_to is not None:
                    next_to = max(0, min(next_to, 3600))
                timed_out = self._cond.wait_for(self.is_timed_out, next_to)
                if timed_out:
                    if self._handle_timed_out():
                        logger.debug('Breaking due to handler telling us so')
                        break
        logger.debug('Stopped')

    def _handle_timed_out(self):
        # check who timed out
        timed_out_proxies = filter(FuncProxy.is_timed_out, self._watched.values())
        if timed_out_proxies:
            try:
                for p in timed_out_proxies:
                    p.timeout_handled()
                return self._callback(*self._args, **self._kwargs)
            except Exception:
                # continue as the timeout was not handled properly...
                # Maybe the next one will be handled properly
                logging.exception('Error while calling callback...')
                return False
        # else: what just happened? Just keep on going
        logger.debug('Unknown timeout...')
        return False

    def stop_thread(self):
        with self._cond:
            self.should_stop = False
            self._cond.notify_all()


def set_timeout_sys_io_to_exit(
    seconds_read=60,
    seconds_out_write=None,
    seconds_err_write=None,
    exitcode=124,
):
    import sys
    import os
    import builtins
    if seconds_read is None:
        raise ValueError('seconds_read cannot be None')
    if seconds_out_write is None:
        seconds_out_write = seconds_read
    if seconds_err_write is None:
        seconds_err_write = seconds_out_write
    if seconds_read < 0 and seconds_err_write < 0 and seconds_out_write < 0:
        raise ValueError('At least one timeout must be >0')

    def exit_func():
        print('timeout')
        os._exit(exitcode)

    detector = TimeoutDetector(exit_func)
    sys.stdin.read = detector.wrap(sys.stdin.read, seconds_read)
    builtins.input = detector.wrap(builtins.input, seconds_read)
    sys.stdout.write = detector.wrap(sys.stdout.write, seconds_out_write)
    sys.stderr.write = detector.wrap(sys.stderr.write, seconds_err_write)
    detector.start()


def patch_sys_in_and_input_to_exit_on_close(exitcode=1):
    import sys
    import os
    import builtins

    # patch sys.in and input to exit on close
    _orig_stdin_read = sys.stdin.read
    _orig_input = builtins.input

    def sys_in_read_exit(*args, **kwargs):
        ret = _orig_stdin_read(*args, **kwargs)
        if len(ret) == 0:
            print('stdin read failed - exiting')
            os._exit(exitcode)
        return ret

    def input_exit(*args, **kwargs):
        try:
            ret = _orig_input(*args, **kwargs)
        except EOFError:
            print('input failed - exiting')
            os._exit(exitcode)
        return ret
    sys.stdin.read = sys_in_read_exit
    builtins.input = input_exit


def patch_oneshot(seconds_timeout=60):
    set_timeout_sys_io_to_exit(seconds_timeout)
    patch_sys_in_and_input_to_exit_on_close()
