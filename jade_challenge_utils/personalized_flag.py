import hashlib
import os
import sys


def get_personalized_flag_env(
        username: str,
        e_hashsecret: str = 'HASH_SECRET',
        e_flag_format: str = 'FLAG_FORMAT',
        hash_algo=hashlib.sha256) -> str:

    hash_secret = os.getenv(e_hashsecret, None)
    if hash_secret is not None:
        # because the secret is given as string, not as hex characters
        hash_secret = hash_secret.encode('utf-8').hex()
    flag_format = os.getenv(e_flag_format, None)
    # hash_secret or flag_format = None is caught in get_personalized_flag
    return get_personalized_flag(username, hash_secret, flag_format, hash_algo)


def get_personalized_flag(
        username: str, hash_secret: str, flag_format: str,
        hash_algo=hashlib.sha256) -> str:

    if username is None:
        raise ValueError('username cannot be None')
    if hash_secret is None:
        raise ValueError('hash_secret cannot be None')
    if flag_format is None:
        raise ValueError('flag_format cannot be None')

    h_input = username.encode('utf-8') + bytes.fromhex(hash_secret)
    return flag_format.format(h=hash_algo(h_input).hexdigest())
