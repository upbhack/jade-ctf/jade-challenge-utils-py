from __future__ import print_function
import sys

if sys.version_info.major == 2:
    def input(*args):
        return raw_input(*args)  # pylint: disable=undefined-variable


class Menu(object):
    def __init__(self, header='', footer='', choice_suffix=')', choice_prefix='', choice_sep=' ', prompt='> ', msg_invalid='Invalid Choice'):
        """Create a simple menu.

        Args:
            header (str, optional): String to be printed before Menu options. Defaults to ''.
            footer (str, optional): String to be printed after Menu options. Defaults to ''.
            choice_suffix (str, optional): String to be printed after choice. Defaults to ')'.
            choice_prefix (str, optional): String to be printed before choice. Defaults to ''.
            choice_sep (str, optional): String to be printed after the suffix. Defaults to ' '.
            prompt (str, optional): String to be printed when expecting input. Defaults to '> '.
            msg_invalid (str, optional): String to be printed on an invalid choice. Defaults to 'Invalid Choice'.
        """
        self.header = header
        self.footer = footer
        self.entries = []
        self.map = {}
        self.choice_prefix = choice_prefix
        self.choice_suffix = choice_suffix
        self.choice_sep = choice_sep
        self.prompt = prompt
        self.msg_invalid = msg_invalid
        self.n = 1

    def element(self, name, *choice, **kwargs):
        """[summary]

        Args:
            name (str): String to display in overview
            *choice (Union[str,int], optional): String/Int to choose this option. Multiple can be given, only first is displayed. Case insensitive. Defaults to last displayorder+1 (if it is an int).
            displayorder (int, optional): Order in which it should be displayed. Must be unique. Lower numbers are displayed before higher ones. Defaults to choice (first int is taken) or last+1.

        Raises:
            ValueError: Invalid choice type
            Exception: Duplicate entries
        """
        displayorder = kwargs.get('displayorder')
        if not choice:
            choice = [self.n]
        for c in choice:
            if not isinstance(c, str) and not isinstance(c, int):
                raise ValueError('Invalid choice type. Must be str or int')
            if displayorder is None and isinstance(c, int):
                displayorder = c
            if str(c) in self.map:
                raise Exception('Duplicate choice entry: '+repr(c))

        def mm_add_element_wrapper(f):
            if displayorder is not None:
                self.n = displayorder
            if self.n in self.entries:
                raise Exception('Duplicate Entry')
            self.entries.append({
                'displayorder': self.n,
                'choice': str(choice[0]),
                'name': name,
            })
            for c in choice:
                self.map[str(c).lower()] = f
            self.entries.sort(key=lambda x: x['displayorder'])
            self.n += 1
            return f
        return mm_add_element_wrapper

    def handle(self):
        if self.header:
            print(self.header)
        for el in self.entries:
            print(
                self.choice_prefix,
                el['choice'],
                self.choice_suffix,
                self.choice_sep,
                el['name'],
                sep='')
        if self.footer:
            print(self.footer)
        choice = str(input(self.prompt)).lower()
        while choice not in self.map:
            print(self.msg_invalid)
            choice = str(input(self.prompt)).lower()
        self.map[choice]()


if __name__ == '__main__':
    '''Testing and "Documentation" :D'''
    M = Menu('Hello World', 'Goodbye')

    @M.element('A', 1)
    def a():
        print("A")

    @M.element('B', 2)
    def b():
        print("B")

    @M.element('Exit', 0, displayorder=999)
    def mm_exit():
        exit(0)

    while True:
        M.handle()
