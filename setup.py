import setuptools
import sys

setuptools.setup(
    name='jade_challenge_utils',
    version='0.0.3',
    author='JADE-CTF Team',
    # author_email
    description='Utilities to ease development of challenges for the JADE CTF Platform',
    # url
    packages=setuptools.find_packages(),
    install_requires=[],
)
