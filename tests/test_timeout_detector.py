#!/usr/bin/env python

import jade_challenge_utils.timeout_detector as td
import random
import time
import pytest


def test_basic_timeout():
    res = []

    def callback(*args, **kwargs):
        res.append((args, kwargs))
        return True

    def a():
        pass

    def b():
        pass

    def c():
        pass

    _args = tuple([random.randint(0, 255) for _ in range(random.randint(2, 10))])
    _kwargs = {'some_key': 'and a value for it', 'r': random.randint(0, 255)}
    t = td.TimeoutDetector(callback, *_args, **_kwargs)
    a = t.wrap(a, 1, 1)
    b = t.wrap(b, 2, 4)
    c = t.wrap(c, 3, 3)
    a()
    b()
    c()
    t.start()
    t.join(timeout=5)
    assert not t.is_alive()
    assert len(res) == 1
    a, kw = res[0]
    assert a == _args
    assert kw == _kwargs
