try:
    import jade_challenge_utils
except ImportError:
    from os.path import dirname as dn
    import sys
    sys.path.append(dn(dn(__file__)))
    import jade_challenge_utils