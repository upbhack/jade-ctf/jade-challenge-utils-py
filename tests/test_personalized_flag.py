#!/usr/bin/env python

import hashlib
import jade_challenge_utils.personalized_flag as pf
import os
import sys
import pytest

TEST_FLAGS = [
    ('usr', 'secret', 'flag{{{h:.8}}}', hashlib.sha256, 'flag{c5fc2159}'),
    ('usr', 'ÄöÜ', 'flag{{_{h:.12}_}}', hashlib.sha256, 'flag{_3e8ba7676f68_}'),
    ('üsr', 'secret', '{h:.16}', hashlib.sha256, 'f0b1426289d8865d'),
]


def test_direct():
    for usr, secret_raw, flag_format, algo, expected_flag in TEST_FLAGS:
        got_flag = pf.get_personalized_flag(
            usr, secret_raw.encode('utf-8').hex(), flag_format, algo
        )
        assert expected_flag == got_flag, (
            'Got different flag; User={usr}, Secret (raw)={secret_raw}, '
            'Format={flag_format}, Algo={algo}: Got {got_flag}, expected {expected_flag}'
        ).format(**locals())


@pytest.mark.parametrize(
    "env_secret,env_format",
    [
        ('SECRET', 'FORMAT'),
        ('HASH_SECRET', 'FLAG_FORMAT'),
        ('FoO', 'BaR')
    ])
def test_env(env_secret, env_format):
    for usr, secret_raw, flag_format, algo, expected_flag in TEST_FLAGS:
        os.environ[env_secret] = secret_raw
        os.environ[env_format] = flag_format
        got_flag = pf.get_personalized_flag_env(usr, env_secret, env_format, algo)
        assert expected_flag == got_flag, (
            'Got different flag; User={usr}, Secret (raw)={secret_raw} in {env_secret}, '
            'Format={flag_format} in {env_format}, Algo={algo}: Got {got_flag}, expected {expected_flag}'
        ).format(**locals())
