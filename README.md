# JADE CTF (python) challenge utils

Utilities to ease development of challenges for the JADE CTF Platform.
These are written for python 3.6+, though they *might* be compatible with pyhton2 if you remove the type hints.
